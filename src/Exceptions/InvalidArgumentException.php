<?php

namespace Cuiqiqing\Payment\Exceptions;

/**
 * 支付参数异常类
 * Class InvalidArgumentException
 * @package Cuiqiqing\Payment\Exceptions
 */
class InvalidArgumentException extends \InvalidArgumentException
{
}
