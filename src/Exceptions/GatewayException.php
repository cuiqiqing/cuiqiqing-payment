<?php

namespace Cuiqiqing\Payment\Exceptions;

/**
 * 支付网关异常类
 * Class GatewayException
 * @package Cuiqiqing\Payment\Exceptions
 */
class GatewayException extends Exception
{
    /**
     * error raw data.
     * @var array
     */
    public $raw = array();

    /**
     * GatewayException constructor.
     * @param string $message
     * @param int $code
     * @param array $raw
     */
    public function __construct($message, $code, $raw = array())
    {
        parent::__construct($message, intval($code));
        $this->raw = $raw;
    }
}
