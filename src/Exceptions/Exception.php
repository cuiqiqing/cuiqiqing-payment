<?php
namespace Cuiqiqing\Payment\Exceptions;

/**
 * 支付异常类
 * Class Exception
 * @package Cuiqiqing\Payment\Exceptions
 */
class Exception extends \Exception
{
}