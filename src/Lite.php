<?php

namespace Cuiqiqing\Payment;

use Cuiqiqing\Payment\Contracts\Config;
use Cuiqiqing\Payment\Contracts\GatewayInterface;
use Cuiqiqing\Payment\Exceptions\InvalidArgumentException;

/**
 * Class Pay
 * @package Pay
 */
class Lite
{

    /**
     * @var Config
     */
    private $config;
	/**
     * @var Config
     */
	private $config_key;

    /**
     * @var string
     */
    private $drivers;

    /**
     * @var string
     */
    private $gateways;

    /**
     * Pay constructor.
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        $this->config = new Config($config);
    }

    /**
     * 指定驱动器
     * @param string $driver
     * @return $this
     */
    public function driver($driver,$config_key = '')
    {
		$this -> config_key = $config_key == '' ? $driver:$config_key;
        if (is_null($this->config->get($this -> config_key))) {
            throw new InvalidArgumentException("Driver [$driver]'s Config is not defined.");
        }
        $this->drivers = $driver;
        return $this;
    }

    /**
     * 指定操作网关
     * @param string $gateway
     * @return GatewayInterface
     */
    public function gateway($gateway = 'web')
    {
        if (!isset($this->drivers)) {
            throw new InvalidArgumentException('Driver is not defined.');
        }
        return $this->gateways = $this->createGateway($gateway);
    }

    /**
     * 创建操作网关
     * @param string $gateway
     * @return mixed
     */
    protected function createGateway($gateway)
    {
        if (!file_exists(__DIR__ . '/Gateways/' . ucfirst($this->drivers) . '/' . ucfirst($gateway) . 'Gateway.php')) {
            throw new InvalidArgumentException("Gateway [$gateway] is not supported.");
        }
        $gateway = __NAMESPACE__ . '\\Gateways\\' . ucfirst($this->drivers) . '\\' . ucfirst($gateway) . 'Gateway';
        return new $gateway($this->config->get($this -> config_key));
    }

}
