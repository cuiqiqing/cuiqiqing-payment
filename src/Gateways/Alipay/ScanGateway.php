<?php

namespace Cuiqiqing\Payment\Gateways\Alipay;

use Cuiqiqing\Payment\Gateways\Alipay;

/**
 * 支付宝扫码支付
 * Class ScanGateway
 * @package Cuiqiqing\Payment\Gateways\Alipay
 */
class ScanGateway extends Alipay
{

    /**
     * 当前接口方法
     * @return string
     */
    protected function getMethod()
    {
        return 'alipay.trade.precreate';
    }

    /**
     * 当前接口产品码
     * @return string
     */
    protected function getProductCode()
    {
        return '';
    }

    /**
     * 应用并返回参数
     * @param array $options
     * @return array|bool
     * @throws \PhalApi\Payment\Exceptions\GatewayException
     */
    public function apply(array $options = array())
    {
        return $this->getResult($options, $this->getMethod());
    }
}
