<?php

namespace Cuiqiqing\Payment\Gateways\Alipay;

use Cuiqiqing\Payment\Gateways\Alipay;

/**
 * 支付宝电子面单下载
 * Class BillGateway
 * @package Cuiqiqing\Payment\Gateways\Alipay
 */
class BillGateway extends Alipay
{

    /**
     * 当前接口方法
     * @return string
     */
    protected function getMethod()
    {
        return 'alipay.data.dataservice.bill.downloadurl.query';
    }


    /**
     * 应用并返回参数
     * @return array|bool
     */
    protected function getProductCode()
    {
        return '';
    }

    /**
     * 应用并返回参数
     * @param array $options
     * @return array|bool
     * @throws \Cuiqiqing\Payment\Exceptions\GatewayException
     */
    public function apply(array $options = array())
    {
        return $this->getResult($options, $this->getMethod(),false);
    }
}