<?php

namespace Cuiqiqing\Payment\Gateways\Wechat;

use Cuiqiqing\Payment\Gateways\Wechat;

/**
 * 微信POS刷卡支付网关
 * Class PosGateway
 * @package PhalApi\Payment\Gateways\Wechat
 */
class PosGateway extends Wechat
{

    /**
     * 当前操作类型
     * @return string
     */
    protected function getTradeType()
    {
        return 'MICROPAY';
    }

    /**
     * 应用并返回参数
     * @param array $options
     * @return array
     * @throws \PhalApi\Payment\Exceptions\GatewayException
     */
    public function apply(array $options = array())
    {
        unset($this->config['trade_type']);
        unset($this->config['notify_url']);
        $this->gateway = $this->gateway_micropay;
        return $this->preOrder($options);
    }

}
