<?php

namespace Cuiqiqing\Payment\Gateways\Wechat;

use Cuiqiqing\Payment\Gateways\Wechat;

/**
 * 微信公众号支付网关
 * Class MpGateway
 * @package Cuiqiqing\Payment\Gateways\Wechat
 */
class MpGateway extends Wechat
{
    /**
     * 当前操作类型
     * @return string
     */
    protected function getTradeType()
    {
        return 'JSAPI';
    }

    /**
     * 设置并返回参数
     * @param array $options
     * @return array
     * @throws \Cuiqiqing\Payment\Exceptions\GatewayException
     */
    public function apply(array $options = array())
    {
    		$result = $this->preOrder($options);
        $payRequest = array(
            'appId'     => $this->userConfig->get('app_id'),
            'timeStamp' => time() . '',
            'nonceStr'  => $this->createNonceStr(),
            'package'   => 'prepay_id=' . $result['prepay_id'],
            'signType'  => 'MD5',
        );
        $payRequest['paySign'] = $this->getSign($payRequest);
		$payRequest['prepay_id'] = $result['prepay_id'];
        $payRequest['trade_type'] = $result['trade_type'];
        return $payRequest;
    }
}
