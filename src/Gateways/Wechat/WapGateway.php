<?php
namespace Cuiqiqing\Payment\Gateways\Wechat;

use Cuiqiqing\Payment\Gateways\Wechat;

/**
 * 微信WAP网页支付网关
 * Class WapGateway
 * @package Cuiqiqing\Payment\Gateways\Wechat
 */
class WapGateway extends Wechat
{

    /**
     * 当前操作类型
     * @return string
     */
    protected function getTradeType()
    {
        return 'MWEB';
    }

    /**
     * 应用并生成参数
     * @param array $options
     * @param string $return_url
     * @return string
     * @throws Cuiqiqing\Payment\Exceptions\GatewayException
     */
    public function apply(array $options = array(), $return_url = '')
    {
        $data = $this->preOrder($options);
        $data['mweb_url'] = isset($data['mweb_url']) ? $data['mweb_url'] : '';
        if (empty($return_url))
            $data['redirect_url'] = $this->userConfig->get('return_url','');
        return $data;
    }
}
