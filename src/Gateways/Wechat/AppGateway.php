<?php

namespace Cuiqiqing\Payment\Gateways\Wechat;

use Cuiqiqing\Payment\Gateways\Wechat;

/**
 * 微信App支付网关
 * Class AppGateway
 * @package PhalApi\Payment\Gateways\Wechat
 */
class AppGateway extends Wechat
{

    /**
     * 当前操作类型
     * @return string
     */
    protected function getTradeType()
    {
        return 'APP';
    }

    /**
     * 应用并返回参数
     * @param array $options
     * @return array
     * @throws \Cuiqiqing\Payment\Exceptions\GatewayException
     */
    public function apply(array $options = array())
    {
        $payRequest = array(
            'appid'     => $this->userConfig->get('app_id'),
            'partnerid' => $this->userConfig->get('mch_id'),
            'prepayid'  => $this->preOrder($options)['prepay_id'],
            'timestamp' => time() . '',
            'noncestr'  => $this->createNonceStr(),
            'package'   => 'Sign=WXPay',
        );
        $payRequest['sign'] = $this->getSign($payRequest);
        return $payRequest;
    }

}
