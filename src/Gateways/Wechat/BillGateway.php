<?php

namespace Cuiqiqing\Payment\Gateways\Wechat;

use Cuiqiqing\Payment\Gateways\Wechat;

/**
 * 下载微信电子面单
 * Class BillGateway
 * @package Cuiqiqing\Payment\Gateways\Wechat
 */
class BillGateway extends Wechat
{

    /**
     * 当前操作类型
     * @return string
     */
    protected function getTradeType()
    {
        return '';
    }

    /**
     * 应用并返回参数
     * @param array $options
     * @return bool|string
     */
    public function apply(array $options)
    {
        unset($this->config['trade_type']);
        unset($this->config['notify_url']);
        $this->config = array_merge($this->config, $options);
        $this->config['sign'] = $this->getSign($this->config);
        return $this->fromXml($this->post($this->gateway_bill, $this->toXml($this->config)));
    }
}