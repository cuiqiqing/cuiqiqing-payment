<?php

namespace Cuiqiqing\Payment\Gateways\Wechat;

use Cuiqiqing\Payment\Gateways\Wechat;

/**
 * 微信扫码支付网关
 * Class ScanGateway
 * @package PhalApi\Payment\Gateways\Wechat
 */
class ScanGateway extends Wechat
{

    /**
     * 当前操作类型
     * @return string
     */
    protected function getTradeType()
    {
        return 'NATIVE';
    }
 
    /**
     * 应用并返回参数
     * @param array $options
     * @return mixed
     * @throws \Cuiqiqing\Payment\Exceptions\GatewayException
     */
    public function apply(array $options = array())
    {
        return $this->preOrder($options)['code_url'];
    }
}
