<?php

namespace Cuiqiqing\Payment\Gateways\Wechat;

use Cuiqiqing\Payment\Gateways\Wechat;

/**
 * 微信小程序支付网关
 * Class MiniappGateway
 * @package Cuiqiqing\Payment\Gateways\Wechat
 */
class MiniappGateway extends Wechat
{

    /**
     * 当前操作类型
     * @return string
     */
    protected function getTradeType()
    {
        return 'JSAPI';
    }

    /**
     * 应用并返回参数
     * @param array $options
     * @return array
     * @throws \PhalApi\Payment\Exceptions\GatewayException
     */
    public function apply(array $options = array())
    {
        $this->config['appid'] = $this->userConfig->get('app_id');
        $payRequest = array(
            'appId'     => $this->config['appid'],
            'timeStamp' => time() . '',
            'nonceStr'  => $this->createNonceStr(),
            'package'   => 'prepay_id=' . $this->preOrder($options)['prepay_id'],
            'signType'  => 'MD5',
        );
        $payRequest['paySign'] = $this->getSign($payRequest);
        return $payRequest;
    }
}
